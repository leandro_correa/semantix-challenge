autor: leandro_correa

Alguns requisitos são necesários para execução completa do código:

PREREQUISITOS

*    JRE (version >= 6);

*    libcurll. Para usuários linux: sudo apt-get install libcurl4-gnutls-dev;

*    libxml. Para usuários linux: sudo apt-get install libxml2-dev;

*    R(version >= 3.2.1)


O código pode ser executado a partir do terminal, ou da interface Rstudio, a partir do script main.R.

Para a visualização da rede foi utilizado a interface R-Java RedeR:

*    https://www.bioconductor.org/packages/release/bioc/html/RedeR.html

Os resultados das análises estão localizados na pasta output da seguinte forma:

*    short_paths_between_vertices.csv: Uma tabela T[i,j], sendo i e j os vértices que compõe a rede, e cada índice [i,j] corresponde a menor distância entre estes vértices;

*   all_Short_Paths: Diretório que contém todos os menores caminhos entre cada vértice da rede. Cada vértice possui um arquivo .txt que indica qual o subconjunto de vértices que pertence a cada menor caminho.

*   Closeness_Centrality.jpg: Barplot dos resultados obtidos da análise da medida closeness centrality de cada um dos vértices da rede. EM vermelho a linha que indica a média dos resultados, e em destaque o menor valor encontrado que indica o vértice mais central da rede.
